﻿using System;

namespace BookEntity
{
    public sealed class Book : IEquatable<Book>, IComparable<Book>
    {
        private bool _published;
        private DateTime _datePublished;
        private int _totalPages;
        
        public string Title { get; }
        
        public int Pages
        {
            get
            {
                return _totalPages;
            }
            
            set
            {
                if (value <= 0)
                {
                    throw new ArgumentOutOfRangeException(nameof(value));
                }

                _totalPages = value;
            }
        }
        
        public string Publisher { get; }
        public string ISBN { get; }
        public string Author { get; }
        public decimal Price { get; private set; }
        public string Currency { get; private set; }
        
        public Book(string author, string title, string publisher)
        {
            Author = author;
            Title = title;
            Publisher = publisher;
        }
        
        public Book(string author, string title, string publisher, string isbn)
            : this(author, title, publisher)
        {
            ISBN = isbn;
        }
        
        public void Publish(DateTime dateTime)
        {
            _published = true;
            _datePublished = dateTime;
        }
        
        public string GetPublicationDate()
        {
            return _published ? _datePublished.ToString("MM/dd/yyyy") : "NYP";
        }
        
        public void SetPrice(decimal price, string currency)
        {
            if (price < 0)
            {
                throw new ArgumentException(nameof(this.Price));
            }

            Currency = currency;
            Price = price;
        }


        
        public int CompareTo(Book book)
        {
            if (book == null)
            {
                return -1;
            }

            if (Title.Equals(book.Title))
            {
                return 0;
            }
            if (Title.Length < book.Title.Length)
            {
                return 1;
            }

            return -1;

        }

        public bool Equals(Book other)
        {
            if (other is null)
            {
                return false;
            }
            
            return this.Author.Equals(other.Author, StringComparison.Ordinal)
                   && this.Title.Equals(other.Title, StringComparison.Ordinal)
                   && this.Publisher.Equals(other.Publisher, StringComparison.Ordinal)
                   && this.ISBN == other.ISBN;
        }


        public override string ToString()
        {
            return Title + " by " + Author;
        }

        public override bool Equals(object obj)
        {
            return obj is Book book 
                   && ISBN == book.ISBN;
        }

        public override int GetHashCode()
        {
            return this._published
                ? (this.Publisher.Length + 1) * (this.Author.Length + this.Title.Length + 1)
                : ((Math.Abs(this.Author.Length - this.Title.Length) + 1) *
                   (this.Author.Length + this.Title.Length + 1)) +
                  this.Publisher.Length;
        }
    }
}